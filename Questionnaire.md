## Questionnaire
**1.** Team: Who needs to be involved?

_CORE TEAM_
- PM: Jacob Schatz

- Dev team: Jacob Schatz - Lead, Ben Hong - Frontend Engineer, Derek Knox - Frontend Engineer, Yannis Roussos - Backend Engineer, Michaël Bergeron - Backend Engieer

- UX: Claudia Petren

_APPROVERS_
- Sid Sijbrandij, Jacob Schatz

**2.** Business Goals: Are we targeting measurable outcomes?
- We are targeting first deliverable outcome. Then measurable outcomes once we get customers.

**3.** User Goals: What do we know about our target users?
- Data people - analysts, scientists, etc.
- Bottom liners - CEOs, Managers, people looking for the bottom line without data knowledge but has a data team.
- Minimal data team - might have a data analyst but no scientists, so no development power, or very little.
- Brand new - no data science knowledge but wants to get data answers.
- (User Research needed - Will create proto-personas, interview target users and validate assumptions.)

**4.** Strategy: Why will people choose us over the alternative?
- Meltano will be the complete solution for data teams.  They will no longer need to jump between multiple tools.
- Complete solution, Open source, Version controllable, Software development principles.

**5.** Tasks & Scenarios: What are the key workflows?
- User Research needed - Will test current application, create sitemap of Meltano and create user journeys to identify key workflows.

**6.** Success measures: Are any key indicators being tracked? What user behaviors translate to profit?
- Access control as version control.

**7.** Key dates and milestones: Are there any significant deadlines that I should be aware of?

**8.** Risks: Are there any significant deadlines redflags that you see upfront?
- TBD