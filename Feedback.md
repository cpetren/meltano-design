## Feedback from User Research

### User 1, 12/10/18

Background: Data Analyst, Automotive industry. Used to work in automotive for about a decade - sales operations, inventory and sales component (best cars to sell based on location)

Started as a Software developer right out of college with automotive company.  Then realized didn’t want to do that and started transitioning in business and analyst. Most recent role, responsible for sales planning and distribution sales.  Research for market opportunities.

Tools used: Business intelligence tool.  Data warehouse. SAP business intelligence, sophisticated and not the easiest to use. Code in sql (database tool). Newer tools that focus on data visualization, Tableau, Vue, Microsoft Power BI.

Challenges: Connecting separate data sets.  1. Customer sales 2. Car maintenance.  
Looking for a tool that allows you to aggregate data in a very flexible way - simple table that you have different models 
Example: Rows - Car types  Columns - months of the year.  I want to have those sales number, but also have information of the sum of the average time cars spent on lot.  Tools are good at manipulating data.  Add other things of data and not just the sum.  Would like to use grouping in flexible way.

Enjoyed:Access to global data set of automotive sales and every brand in the world.  For a person that works with data it’s really rewarding.  

Personal life: Passions is photography, travel is another passion.  Travel with finance quite a bit.  Enjoy music and classical music performance.  Going to concerts is really nice and museums are really nice.  Getting together with friends.

Education is probably the core similarities.  Mathematics, computer science, systems.  Skillset you have to have. Left brain mentality.  Find that folks have passion industry.  Successful data analyst.

Microsoft exec is the most important tool - actually end one of the most flexible tools they. 

### User 2, 12/10/18

Background: Data engineer consultant. Help companies understand why they should use MySQL, Database Oracle, MongoDB.

Started in 2016, some friends talked to me about data consulting.  Still new in this area but has worked on 10 projects.

Mostly reactive when a company is trying to put out a fire.
- Example: Transformation load is taking too much time or having trouble with a query. Mix of dba (database administrator jobs) and data engineer.

Proactive, when a company is developing something new.

Challenges: As a consultant, I have access to the database itself, but not to the application.  Business rules is relevant at times. Sometimes they ask you to do things when they actually need something different. Also, too many different technologies being used right now.

Resolving challenges:  Ask more questions. What is the business issue, intention POV? I.T. intention POV? What are the objectives, target? 

Enjoy: Performance reviews to the clients at the beginning of a project.  They are able to identify so many problems they could easily fix.  Also identify things they can’t fix.

Measuring success: Client satisfaction. They have an issue and we solve.

Personal life: Live in a beach city in Brazil and love to take son to beach.  Like to ride bike with 5 year old son. Not standard data engineer: don’t play any video games, play with son and don’t even have television at home.  Follow politics and news.

### User 3, 12/10/18

Background: Data engineer for almost 3 years

Typical day: Check pipelines, make sure they haven’t failed.  Depends, split time between maintaining infrastructure and adding new functionality for new project.  Work with dev ops and data analyst.

Proactive or reactive: Should be mostly proactive.  If you haven’t done things well, it will be reactive (bad foresight).

Challenges: 1, making sure that everything is reliable.  Data needs to move consistently and be correct.  Must be solid foundation.  If the data isn’t there and inaccurate, there is a huge problem. 2, everything you do has to be extensible and buildable.  Building blocks so can go onto and get more complex.

Solving: Mix of hosted and things that must be done manually through command lines.

Hardest part: Need to focus on how you build.  Keeping things simple and reducing complexity.  If foundation is not solid pipeline will fail.  Each piece has to be solid to prevent compounding work so everything is maintainable.

Resolving instability: Building what you can’t buy or build what you have to borrow what you can.  Using the right tools for the job that are reliable.  Go with the better solution.

Ideal workflow tool set: Stitch, Custom extraction and load solutions, dbt (transformation), Airflow (orchestration), Kubernetis (Ariflow and mashing learning will live)

Enjoy: Seeing things work and not breaking is really satisfying.  Like it when things are moving forward.  The longer the pipelines can go without them breaking the better. I love writing code and solving problems.

Success: How much uptime for the pipeline. Machine learning models in production. Happiness of Analyst. 

Accomplish next year: Implement offline machine learning models in sales prediction. Foundation and pipeline needs to be solid to move forward. Friends in the industry and blogs around machine learning will make it easier to accomplish goal.  Can’t foresee anything that will prevent achieving goal.

Personal life: Wife and kid. Play drums, go to gym.  Nothing too crazy.  Spend a lot of time with family.  Simple things.

### User 4, 12/11/18

Background: Data analytics for 3.5 years. Politics major in college (political methodology) ask question, run experiment. Developed habit of asking questions and data sets. Double down on tech working at startup after college. Learned open source DBT. 

Workday: Receive request. Use looker as data visualization tool. Build all code behind looker - build architecture that people can review. Once it's in the warehouse that is when Analyst comes in. Goal is How to make it easy for other people to consume.  Work with data engineer the most.

Proactive or reactive: Both. Reactive, responding to request and Proactive due to getting ready to hire.

Challenges: So much work not enough bandwidth. Lack of existing data knowledge. Frustation with existing team (Looker, version control is poor) open source dbt. Pipeline (CI) is frustrating. Data team is really far behind. Educating consumers "How to ask so I can Answer them"

Enjoy: Paid to think. New challenge. Impactful and valued. 5 star boss.

Success: Meeting my quarterly goals

Goal: Learn more about Data engineering

### User 5, 12/11/18

Background: Data engineer 2.5 years

Workday:  Everyday is different.  Generally, work from a series of Jupyter Notebooks online and do various projects and experiments from those to make better predictions.  Sometimes working on theory and sometimes working on implementation.

Challenges: Getting experiment that was done into an implemented algorithm is a big pain-point.  Have to do transformation on stuff that is in the database and right now the transformation really stink. They take a ton of time to run and sometimes they have to be modified. Big json objects that need to be converted into simplified data structure.  Along the way there are a lot of missing values.  There are too many notebooks and too many incompatibilities with earlier iterations and not enough documentation.

Solution: I’d like to see the documentation for the standard data types enforced continuously somehow.  If I knew I could query a service and get this one object type and it would automatically integrate and actually forced to work.  Would love some system that queries our database, gets those objects, checks and makes it easy to import.

### User 6, 12/11/18

Background: Data scientist and engineer for over 5 years.  Started her career as an engineer and always liked data.

Day to day: Varies.  Meetings, DataGrip (UI where you write sql), analysis in python, Excel.  Work with PM’s mostly.

Challenges: Accessing data.  Raw data and tables that need to be built.  Data is there, but then it has to be built out.  Small company so not a lot of data and some of the data is not logged.  Building a reasonable model is also challenging. Streamlining workflow and getting that integrated with data base. Have to write a lot of sql.

Ideal solution: Don’t really know.  Still fragmentation in many tools.  Tools are made for specific use cases and not end to end. I would have to start with the source. 1. Easy way to know if source of data is updated to know if things are failing and what the dependencies upstream. 2. Quick checking.  Light weight spot checking, checking tables.  Not a full blown feature. Just query and get results back. 3. More complicated query IDE would be helpful. 4. Simple graph visualization. 5. Building models in python. 6. Recurring job and pipeline itself.  Maybe simple click button and know that everything will work. 7. Python notebook.

Enjoy: People, learning a lot from engineers and great company.

Personal life: Married with 1 year old toddler. Busy raising her and exploring California on weekends.

### User 7, 12/14/18

Background: Data engineer, about 5yrs. Studied computer science in University.

Day to day: We support research teams and product teams, providing the data in the correct form.  We ingest a lot of data, more than 4 terabits a day. We need to process, read, transform, validate on all this data that we are ingesting.  We then extract some metrics and provide in a proper way. 

Currently, we are setting up the infrastructure on the data platform we are joining the dodge between engineers on product (android, mobile, web) that are presenting the data with other research.  We have a good research team that provides models to understand all the user behaviors and recommendations.  We are the bridge between engineers and research.  Work with backend engineers because they are the ones that send us the data.  We have some tools that define data schemas; how the data should be, so we can validate and trust data.  If the data is 100% okay then we can trust and then build proper models and products on top of that.  With research we are providing different pipelines.  Right now I am using Airflow. I’ve used Luigi in the past. Currently only work with Data Scientist.

Current team: 3 Data engineers, 2 teams of research. 15 Data scientists total.

Challenges: We are moving from prototyping to production.  Then you have from one side the libraries that you are producing (building a model, collaboration, filtering). But then when you put it in production, things don’t work.  It’s different pipelines because you have libraries from one side and an other side you are building the pipeline to ingest data to do the recommendation to store it somewhere.  Then you have in another point the API that you are serving these recommendation.  Then you have to integrate with DataDoc.  Many points. 

Resolving challenges:  Currently trying to integrate DataDocs everywhere so we have metrics about everything. Then we can see the problem.  We test, but sometimes there are use cases that have not been covered. So then we try to reproduce in a small environment to see what is causing this.

Ideal solution: I don’t know right now.  From one side, a lot of research are using Jupiter Notebooks, but then is not production.  It’s more for adhock analysis, some testing.  And also then, when you put pipelines into production like Airflow you need other third party to be the performance. Right now Airflow and Luigi just inform us if it’s okay and how much time was consumed, but nothing else.  We need to add more things inside to see what is going on. So maybe we can integrate DataDocs with Airflow all together and having a way to define more metrics in the same platform.  Instead of having different infrastructures.  That would be better.  Imagine we could integrate with Kubernetes someway and then you have everything in one place.

Motivations: At the end, seeing that everything works.  That we solved having a prototype to something in production. So in the end so the users are getting benefits.  When the full pipeline is working, then I am happy.

Frustrations:  When something is failing and you can not see where.

Enjoy: Productionizing. 

Measuring success: When everything works.  Expected resources. Track uptime with Datadocs.

Goals:  Idea is to have 99% of pipelines up.  And have alerting systems.  Tools to research.  Lack of knowledge would prevent team from meeting goals.

Personal life: Used to play lacrosse.Sometimes write some tutorials about spark or machine learning. Learning new tech things.

### User 8, 1/09/19

Background: Machine learning Research Engineer. Researching on machine learning models. My task is to find ways to explain those models to humans. PHD in theoretical physics. I like doing math, I like programing, and that drove me into this field.

Day to day: Reading papers and building prototypes for a system similar to ‘yuber’.  For fast prototyping I used python. And for proof of concept I used Doland and C++.  Part of my job was to build visualizations to take to the client and sell the product.  So the client can see how things are working. I use packages from python and made some pdf.  During my day I read papers, try to fix things and make simulations to explain to potential customers.

Teams: Work with software engineering team and CEO and people that sell the product. 

Both reactive and proactive role.  Set and define problems for the company and then find ways to solve that. Pushing company to optimize.  But when clients come with requests my role is more reactive. Building tools to explain to customers.

Challenges: We have a problem with data privacy to protect users data for GDPR.  Currently just doing a lot of reading and researching.  Probably for about a year.

Enjoy most:  The smart people I work with.  Different people and different backgrounds.  Making proof of concepts.

Measure success: OKR every quarter.  Set goals for the product.

Personal life:  Like to bike.  Bikes every day to work. Swimming.  Love going to the theater.  Twice a year go to theater. Beer.
